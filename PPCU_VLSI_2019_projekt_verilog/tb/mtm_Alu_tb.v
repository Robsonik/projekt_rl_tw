`timescale 1ns / 1ps

module mtm_Alu_tb (
    input wire sin,
    output reg clk,
    output reg rst_n,
    output reg sout     
);

 
reg baudClk; 
// Declare a clock period constant.
localparam 
    ClockPeriod = 20,
    TICK_PER_BIT = 10,
    baudClkPeriod = ClockPeriod * TICK_PER_BIT,
    AND = 3'b000,
    OR = 3'b001,
    ADD = 3'b100,
    SUB = 3'b101
    ;

reg crcERR=0;
reg op=0;
reg frameErr=0;
wire [2:0]ERR_FLAGS;
assign ERR_FLAGS = {frameErr, crcERR, op};

reg [31:0] k, a,b;
initial 
    begin
    #(100);
    k=0;
    a=0;
    b=0;
    sout = 1'b1;
    clk = 1'b1;
    baudClk = 1'b0;
    rst_n = 1'b0;
    #(baudClkPeriod)
rst_n = 1'b1;
    #(baudClkPeriod);
    
    crcERR=1;
    op=0;
    frameErr=0;
    $display("sending crc err");
    send_calculation_data(32'b11111111_11111111_00000000_10101010,32'b11111111_00000000_11111111_10101010,AND,1);//crcerr
    rst_n = 1'b0;
    #(baudClkPeriod)
rst_n = 1'b1;
    #(baudClkPeriod);
    crcERR=0;
    op=1;
    frameErr=0;
    $display("sending op err");
    send_calculation_data(32'b11111111_11111111_00000000_10101010,32'b11111111_00000000_11111111_10101010,3'b010,0);//operr
    rst_n = 1'b0;
    #(baudClkPeriod)
rst_n = 1'b1;
    #(baudClkPeriod);
    crcERR=0;
    op=0;
    frameErr=1;
    $display("sending 7 data byte ");
    sendByte(0,0);
    sendByte(0,0);
    sendByte(0,0);
    sendByte(0,0);
    sendByte(0,0);
    sendByte(0,0);
    sendByte(0,0);
    sendByte(1,0);//frame err
    compareResult(0,0,0);
    
    rst_n = 1'b0;
    #(baudClkPeriod)
rst_n = 1'b1;
    #(baudClkPeriod);
    
    $display("corner test");
    send_calculation_data(0,0,AND,0);
    send_calculation_data(0,32'hFFFFFFFF,AND,0);
    send_calculation_data(32'hFFFFFFFF,0,AND,0);
    send_calculation_data(32'hFFFFFFFF,32'hFFFFFFFF,AND,0);
    $display("AND ok");
    
    send_calculation_data(0,0,OR,0);
    send_calculation_data(0,32'hFFFFFFFF,OR,0);
    send_calculation_data(32'hFFFFFFFF,0,OR,0);
    send_calculation_data(32'hFFFFFFFF,32'hFFFFFFFF,OR,0);
    $display("OR ok");
    
    send_calculation_data(0,0,ADD,0);
    send_calculation_data(0,32'hFFFFFFFF,ADD,0);
    send_calculation_data(32'hFFFFFFFF,0,ADD,0);
    send_calculation_data(32'hFFFFFFFF,32'hFFFFFFFF,ADD,0);
    send_calculation_data(32'hFFFFFFFF,32'hFFFFFFFF,ADD,0);
    $display("ADD ok");
    send_calculation_data(0,0,SUB,0);
    send_calculation_data(0,32'hFFFFFFFF,SUB,0);
    send_calculation_data(32'hFFFFFFFF,0,SUB,0);
    send_calculation_data(32'hFFFFFFFF,32'hFFFFFFFF,SUB,0);
    $display("SUB ok");
    a=0;
    b=0;
    for(k=0;k<=1024; k=k+1)
        begin 
        a=$random(b);
        b=$random(a);
        send_calculation_data(b,a,AND,0);
        send_calculation_data(b,a,OR,0);
        send_calculation_data(b,a,ADD,0);
        send_calculation_data(b,a,SUB,0);
        if (k[6:0]==7'b0)$display("run:%d\t a:%h \t b:%h\t passed",k,a,b);
        end
    $display("TEST PASSED");
    $display("Finish");
    $finish;
    end
initial 
begin  
  #(300000000)$display("timeout, STOP");
    $finish;
end    
    
// Clock Generation
always  #(ClockPeriod / 2) clk = ~clk;
always  #(baudClkPeriod / 2) baudClk = ~baudClk;

wire serialOutWire;
 
task sendByte; // task definition starts here
input isCTL;
input [7 : 0] dIn;
integer k;
    begin
    sout = 1'b0;
    #(baudClkPeriod)
    sout = isCTL;
    for (k=7; k >= 0; k = k -1)
        begin
        #(baudClkPeriod)
        sout = dIn[k];
        end
    #(baudClkPeriod);
     sout = 1'b1;
    #(baudClkPeriod);
    end
endtask // task definition ends here


task send_calculation_data; // task definition starts here
input [31:0] B;
input [31:0] A;
input [2:0] OP;
input [2:0]crcERR;
reg [67:0] d;
reg [3:0] c;
reg [3:0] newcrc;
 begin
 //crc from web generator
 d = {B, A, 1'b1, OP};
 c = 4'b0000;
 newcrc[0] = d[66] ^ d[64] ^ d[63] ^ d[60] ^ d[56] ^ d[55] ^ d[54] ^ d[53] ^ d[51] ^ d[49] ^ d[48] ^ d[45] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[36] ^ d[34] ^ d[33] ^ d[30] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[18] ^ d[15] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[0] ^ c[0] ^ c[2];
 newcrc[1] = d[67] ^ d[66] ^ d[65] ^ d[63] ^ d[61] ^ d[60] ^ d[57] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[45] ^ d[42] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[31] ^ d[30] ^ d[27] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[12] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[1] ^ d[0] ^ c[1] ^ c[2] ^ c[3];
 newcrc[2] = d[67] ^ d[66] ^ d[64] ^ d[62] ^ d[61] ^ d[58] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[47] ^ d[46] ^ d[43] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[34] ^ d[32] ^ d[31] ^ d[28] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[16] ^ d[13] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[2] ^ d[1] ^ c[0] ^ c[2] ^ c[3];
 newcrc[3] = d[67] ^ d[65] ^ d[63] ^ d[62] ^ d[59] ^ d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[48] ^ d[47] ^ d[44] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[35] ^ d[33] ^ d[32] ^ d[29] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[14] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ c[1] ^ c[3];
 /*
 * 1. DATA( B[31:24] )
 * 2. DATA( B[23:16] )
 * 3. DATA( B[15:8] )
 * 4. DATA( B[7:0] )
 * 5. DATA( A[31:24] )
 * 6. DATA( A[23:16] )
 * 7. DATA( A[15:8] )
 * 8. DATA( A[7:0] )
 * 9. CTL( {1'b0, OP, CRC} )
 */
    sendByte(0, B[31:24]);
    sendByte(0, B[23:16]);
    sendByte(0, B[15:8]);
    sendByte(0, B[7:0]);
    sendByte(0, A[31:24]);
    sendByte(0, A[23:16]);
    sendByte(0, A[15:8]);
    sendByte(0, A[7:0]);
    sendByte(1, {1'b1, OP, newcrc+crcERR});
    compareResult(B,A,OP);
 end
 endtask // task definition ends here
 
 
 task compareResult; // task definition starts here
 input [31:0] B;
 input [31:0] A;
 input [2:0] OP;
 
  integer k, timeout;
  reg packetEnd;
  reg [45:0] result, testResult;
  //reg [7:0] ctl
  reg cmd;
  reg [2:0] crc;
  reg zero, carry, ovf, negative; 
  reg [3:0] FLAGS;
  reg [3:0] bytes;
  reg [35:0] tmp;
   
  begin
  packetEnd = 0;
  crc = 0;
  bytes =0;
  result=0;
  timeout =0;

  while ((packetEnd == 0) && (1 < 10000))//wait for CTL frame 
        begin
        while((sin == 1'b1) && (timeout < 10000))
            begin
            #(ClockPeriod); // wait for start bit
            timeout = timeout +1;
            end//end while(sin == 1'b1 && timeout < 1000)
        #(baudClkPeriod/2);// wait for sample data
        //START BIT
        #(baudClkPeriod);
        cmd = sin;
        packetEnd = sin;//1- CTL 0- DATA
        for (k=7; k >=0; k = k -1) 
            begin
            #(baudClkPeriod);// wait for sample data
            crc = {crc[1]^sin, crc[0], crc[2]^sin};
            result= {result[44:0], sin};
            end
        bytes = bytes + 1;
        #(baudClkPeriod);//STOP    
        if (timeout < 9999)    timeout =0;
        end//end while (packetEnd == 0 && timeout < 1000)//wait for CTL frame 
    
    if (timeout >= 9999)
	begin
            $display("receiver timeout, FAIL");
            $stop;
	end


        if (bytes >5)
            begin
            $display("to many data bytes");
            $display("FAIL");
            //$stop;
            end
        
        case(OP)
            3'b000://and
            begin
                testResult = A&B;
                negative = testResult [31];
                zero =    (testResult == 0 );
                ovf = 0;
                carry = 0;
            end 
            3'b001://or
            begin
                testResult = A|B;
                negative = testResult [31];
                zero =    (testResult == 0 );
                ovf = 0;
                carry = 0;
            end 
            3'b100://sum
            begin 
                testResult = A+B;
                carry= testResult[32];
                zero = (testResult == 0 );
                negative = testResult [31];             
                tmp = A[30:0]+B[30:0];
                ovf = (carry ^ tmp[31]);       
            end
            3'b101://sub
            begin
                testResult = A-B;
                carry= testResult[32];
                zero = (testResult == 0 );
                negative = testResult [31];
                tmp = A[30:0]-B[30:0];
                ovf = (carry ^ tmp[31]);   
            end
            default:
            begin//test wrong op 
            end
         endcase 
       // end//end data reading loop
            
        if (bytes == 5)
            begin
            FLAGS[3] = negative;
            FLAGS[2] = zero;
            FLAGS[1] = ovf;
            FLAGS[0] = carry;
            if(FLAGS != result[6:3])
                begin
                $display("flags ERRtest:%b received:%b",FLAGS, result[6:3] );
 		$display("result ERR test:%b received:%b",testResult,result>>8);
		$display("result:A %b B:%b OP:%b",A,B,OP);
                $display("FAIL");
                $stop;
                end
            if(testResult[31:0] != result>>8)
                begin
 		$display("flags ERR test:%b received:%b",FLAGS, result[6:3] );
                $display("result ERR test:%b received:%b",testResult,result>>8);
		$display("result:A %b B:%b",A,B);
                $display("FAIL");
                $stop;
                end             
            end
        else if (bytes == 1)//error packet
            if (result[7:0] == {1'b1, ERR_FLAGS, ERR_FLAGS,1'b1})
                begin
                $display("received correct error frame");
                end
            else
                begin
                $display("error packet test fail");
     		$display("flags ERR test:%b received:%b",{1'b1, ERR_FLAGS, ERR_FLAGS,1'b1}, result[7:0]);
                $display("result ERR test:%b received:%b",testResult,result>>8);
		$display("result:A %b B:%b",A,B);
                $display("FAIL");
                $stop;
                end 
        else
            begin
            $display("byte count ERR");
            $display("FAIL");
            $stop;
            end      
        end

  endtask 
endmodule
