#####################################################################
#
# Init setup file
# Created by Genus(TM) Synthesis Solution on 09/12/2019 15:22:59
#
#####################################################################


read_mmmc RESULTS/mtm_Alu.mmmc.tcl

read_physical -lef {/cad/dk/umc180/SUS/SUSLIB_UCL_tech.lef /cad/dk/umc180/SUS/SUSLIB_UCL.lef}

read_netlist RESULTS/mtm_Alu.v

init_design
