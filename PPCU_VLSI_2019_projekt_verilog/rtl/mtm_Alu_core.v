`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.09.2019 18:52:31
// Design Name: 
// Module Name: mtm_Alu_pararelAlu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module mtm_Alu_core (
    input  wire clk,   // posedge active clock
    input  wire rst_n, // synchronous reset active low
	input  wire [31:0] A,
	input  wire [31:0] B,
	input  wire [2:0] OP,
	input  wire dataReadyIn,
	output reg  dataReadyOut,
    output reg  [31:0] C,   // serial data output
	output reg  OP_ERROR,
	output wire [3:0] FLAGS	
);

	reg [31:0] C_next;
	reg OP_ERROR_next, Carry_in_next, dataReadyOut_next;
	reg Negative, Negative_next, Zero, Zero_next, Overflow, Overflow_next, Carry_out, Carry_out_next;
    
    assign FLAGS[3] = Negative;
    assign FLAGS[2] = Zero;
    assign FLAGS[1] = Overflow;
    assign FLAGS[0] = Carry_out;
    
    wire signed [32:0] AC;
    wire signed [32:0] BC;
    assign AC = {1'b0,A}; 
    assign BC = {1'b0,B};

    always @(posedge clk)
    if(rst_n == 1'b0)
        begin
        C <= 0;
        OP_ERROR <= 1'b0;
        Negative <= 1'b0;
        Zero <= 1'b0;
        Overflow <= 1'b0;
        Carry_out <= 1'b0;
        dataReadyOut <= 1'b0;
        end
    else
        begin
        C <= C_next;
        OP_ERROR <= OP_ERROR_next;
        Negative <= Negative_next;
        Zero <= Zero_next;
        Overflow <= Overflow_next;
        Carry_out <= Carry_out_next;
        dataReadyOut <= dataReadyOut_next & dataReadyIn;
        end
    
    always @(*)
     if(C_next == 0)
        Zero_next = 1'b1;
     else 
        Zero_next = 1'b0;
    
    always @(*)
     if(C_next[31] == 1'b0)
        Negative_next = 1'b0;
     else
        Negative_next = 1'b1;
        
    always @ (*)
         begin   
          case(OP)
             3'b000: 
                 begin 
                  C_next = A & B;
                  OP_ERROR_next = 1'b0;
                  Carry_in_next = 1'b0;
                  Carry_out_next = 1'b0;
                  Overflow_next = 1'b0;
                  dataReadyOut_next = 1'b1;
                 end
             3'b001: 
                 begin 
                  C_next = A | B; 
                  OP_ERROR_next = 1'b0;
                  Carry_in_next = 1'b0;
                  Carry_out_next = 1'b0;
                  Overflow_next = 1'b0;
                  dataReadyOut_next = 1'b1;
                 end
             3'b100: 
                 begin 
                  C_next = A + B; 
                  OP_ERROR_next = 1'b0;
                  Carry_in_next = {{1'b0,A[30:0]} + {1'b0,B[30:0]} >> 31};// [31];
                  Carry_out_next = {AC+BC}>>32;
                  Overflow_next = (Carry_in_next ^ Carry_out_next);
                  dataReadyOut_next = 1'b1;
                 end
             3'b101: 
                 begin 
                  C_next = A - B;
                  OP_ERROR_next = 1'b0;
                  Carry_in_next = {{{1'b0,A[30:0]} - {1'b0,B[30:0]}}>>31};
                  Carry_out_next = {AC-BC}>>32;
                  Overflow_next = (Carry_in_next ^ Carry_out_next);
                  dataReadyOut_next = 1'b1;
                 end
             default:
                 begin
                  C_next = 0;
                  Carry_in_next = 1'b0;
                  Carry_out_next = 1'b0;
                  OP_ERROR_next = 1'b1;
                  Overflow_next = 1'b0;
                  dataReadyOut_next = 1'b0;
                  end 
          endcase
         end  
endmodule 
