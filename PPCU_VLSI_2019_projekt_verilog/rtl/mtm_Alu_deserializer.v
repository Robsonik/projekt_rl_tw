`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.08.2019 11:16:05
// Design Name: 
// Module Name: deserializer_core
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module mtm_Alu_deserializer(

    input wire clk,
    input wire rst_n,
    input wire sin,
    output  reg  dataReady,
    output  reg  [31:0] A_OUT,   
    output  reg  [31:0] B_OUT,    
    output  reg  [2:0] OP_OUT,   
    output  reg  ERR_CRC,    // bad CRC for the input data
    output  reg  ERR_DATA   // incorrect input data, wrong number of DATA frames before CTL frame (should always be 8),   
    );
    
    reg [31:0] a_, a_nxt, b_, b_nxt;
    reg [2:0] op_, op_nxt;
    reg [7:0] ctl, ctl_nxt;
    
    reg  dataReady_nxt, ERR_CRC_nxt, ERR_DATA_nxt;
    reg  [31:0] A_OUT_nxt;   
    reg  [31:0] B_OUT_nxt;    
    reg  [2:0] OP_OUT_nxt;
    
    localparam 
        TICK_PER_BIT=10
     ;
     
     reg errorFrame, errorFrame_nxt;
     reg [4:0] byteCnt, byteCnt_nxt; 
     reg cmd, cmd_nxt;
     reg [4:0]state, state_nxt;
     reg [4:0] bitCnt, bitCnt_nxt;
     
     localparam 
         //BIT_START = 5'b00001, GET_CMD = 5'b00010, GET_BIT = 5'b00100, ERROR_FRAME = 5'b01000, STOP = 5'b10000
         BIT_START =5'd1, GET_CMD = 5'd2, GET_BIT = 5'd4, ERROR_FRAME = 5'd8, STOP = 5'd16
         ;
         
     wire [67:0] d;
     wire [3:0] c;
     wire [3:0] newcrc;
           //code from web crc gen
     assign d = {b_, a_, 1'b1, op_};
     assign c = 4'b0;//ctl[3:0];
     assign newcrc[0] = d[66] ^ d[64] ^ d[63] ^ d[60] ^ d[56] ^ d[55] ^ d[54] ^ d[53] ^ d[51] ^ d[49] ^ d[48] ^ d[45] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[36] ^ d[34] ^ d[33] ^ d[30] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[18] ^ d[15] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[0] ^ c[0] ^ c[2];
     assign newcrc[1] = d[67] ^ d[66] ^ d[65] ^ d[63] ^ d[61] ^ d[60] ^ d[57] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[45] ^ d[42] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[31] ^ d[30] ^ d[27] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[12] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[1] ^ d[0] ^ c[1] ^ c[2] ^ c[3];
     assign newcrc[2] = d[67] ^ d[66] ^ d[64] ^ d[62] ^ d[61] ^ d[58] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[47] ^ d[46] ^ d[43] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[34] ^ d[32] ^ d[31] ^ d[28] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[16] ^ d[13] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[2] ^ d[1] ^ c[0] ^ c[2] ^ c[3];
     assign newcrc[3] = d[67] ^ d[65] ^ d[63] ^ d[62] ^ d[59] ^ d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[48] ^ d[47] ^ d[44] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[35] ^ d[33] ^ d[32] ^ d[29] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[14] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ c[1] ^ c[3];
     
     
//#########################      baud and reset generator    #########################

    reg baudGenEn, baudGenEn_nxt, baudClk, baudClk_nxt;
    reg [3:0] baudGenCnt, baudGenCnt_nxt;
    //assign rstOut = ~baudReset;
    assign rstOut = rst_n;
    always @( posedge clk )
    begin
    /* generator run if 
    sin is stable low for half clock cycle. Then state machine start and set baudGenEn
    Synchronous reset reqiured at last one clock cycle from baud generator
        enable from state machine        start              reset generator */
    if  ((( baudGenEn == 1'b1 ))   ||   ( sin == 1'b0 )) //
        begin
        baudGenCnt <= baudGenCnt_nxt;
        baudClk <= baudClk_nxt;
        end
    else
        begin
        baudGenCnt <= TICK_PER_BIT/2;
        baudClk <=1'b0;
        end
    end    
    // set reset request reg    
    /*
    always @(*) 
    begin   
    if (rst_n == 1'b0) baudReset_nxt = 1'b1;
    else if ((baudGenCnt_nxt == 4'b0) && ( baudClk_nxt == 1'b0)) baudReset_nxt = 1'b0;
    else baudReset_nxt = baudReset;
    end
      */ 
    always @( posedge clk ) 
    if (rst_n == 1'b0) 
    begin 
        A_OUT <= 32'b0;
        B_OUT <= 32'b0;
        OP_OUT <= 3'b0;
        baudGenEn <= 1'b0;
        errorFrame <= 1'b0;
    end
    else
    begin      
         A_OUT <= A_OUT_nxt;
         B_OUT <= B_OUT_nxt;
         OP_OUT <= OP_OUT_nxt;
         baudGenEn <= baudGenEn_nxt;
         errorFrame <= errorFrame_nxt;
    end
    
    always @*
    if (baudGenCnt >= TICK_PER_BIT-1)
        begin
        baudGenCnt_nxt=4'b0;
        baudClk_nxt = 1'b1;
        end
    else
        begin
        baudGenCnt_nxt = baudGenCnt + 4'b1;
        baudClk_nxt = 1'b0;        
        end
     
//#########################     bit read            #########################  
        
    //always @( posedge baudClk) //
    always @( posedge clk )
    begin 
        if(rst_n == 1'b0)
        begin
            //errorFrame <= 0;
            byteCnt <= 5'b0;
            a_ <= 32'b0;
            b_ <= 32'b0;
            op_ <= 3'b0;
            ctl <= 8'b0;
            
            state <= BIT_START;
            cmd <= 1'b0;
            bitCnt <= 5'b0;
            byteCnt <= 5'b0;
            dataReady <= 1'b0;
            ERR_CRC <= 1'b0;
        end
        else 
        begin
            byteCnt <= byteCnt_nxt;
            a_ <= a_nxt;
            b_ <= b_nxt;
            op_ <= op_nxt;
            ctl <= ctl_nxt;
            
            state <= state_nxt;
            cmd <= cmd_nxt;
            bitCnt <= bitCnt_nxt;
            byteCnt <= byteCnt_nxt;
            dataReady <= dataReady_nxt;
            ERR_CRC <= ERR_CRC_nxt;
        end
    end    
        
always @*
    begin 
    if (baudClk == 1'b1)
        case(state)
            BIT_START : // must be true because start contition was checked 
                begin
                state_nxt = GET_CMD;
                baudGenEn_nxt = 1'b1; //wait for start
                cmd_nxt = cmd;
                bitCnt_nxt = 5'b0;
                end
            GET_CMD :
                begin
                state_nxt = GET_BIT;
                cmd_nxt = sin;
                baudGenEn_nxt = 1'b1;
                bitCnt_nxt = 5'b0;
                end
            GET_BIT :
                begin
                cmd_nxt = cmd;
                baudGenEn_nxt = 1'b1; 
                if (bitCnt >= 7)
                    begin
                    bitCnt_nxt = 5'b0;
                    state_nxt = STOP;    
                    end
                else
                    begin
                    bitCnt_nxt = bitCnt+1;
                    state_nxt = GET_BIT;
                    end
                end
            STOP:
                begin
                bitCnt_nxt = 5'b0;
                if (( errorFrame == 1'b1 ) || (sin != 1'b1))state_nxt = ERROR_FRAME; //data lenght and stop bit check
                else state_nxt = BIT_START; 
                cmd_nxt = cmd;
                baudGenEn_nxt = 1'b0;                    
                end
            ERROR_FRAME :
                begin
                bitCnt_nxt = 5'b0;
                state_nxt = ERROR_FRAME;
                cmd_nxt = cmd;
                baudGenEn_nxt = 1'b0; 
                end
            default : 
                begin
                bitCnt_nxt = 5'b0;
                state_nxt = BIT_START;
                cmd_nxt = 1'b0;
                baudGenEn_nxt = 1'b0; 
                end
        endcase
    else//wait for baud clock
        begin
        bitCnt_nxt = bitCnt;
        state_nxt = state;
        cmd_nxt = cmd;
        baudGenEn_nxt =baudGenEn; 
        end
    end          
    
//#########################     byte read       #########################  
    
always @*
begin
    if (baudClk == 1'b1)
    begin
        if (state == GET_BIT)
            begin
            dataReady_nxt = 1'b0;
            ERR_CRC_nxt = ERR_CRC;
            byteCnt_nxt = byteCnt;
            A_OUT_nxt = A_OUT;
            B_OUT_nxt = B_OUT;
            OP_OUT_nxt = OP_OUT;
            if (byteCnt < 4)// shift B 
                begin
                if (cmd == 1'b1)  errorFrame_nxt = 1'b1;
                else errorFrame_nxt = errorFrame;
                a_nxt = a_;
                b_nxt = {b_[30:0], sin};
                op_nxt=op_;
                ctl_nxt = 8'b0;
                end
            else if (byteCnt < 8)// shift A
                begin
                if (cmd == 1'b1) errorFrame_nxt = 1'b1;
                else errorFrame_nxt = errorFrame;
                a_nxt={a_[30:0],sin};
                b_nxt=b_;
                op_nxt = op_;
                ctl_nxt = 8'b0;
                end
            else if (byteCnt == 8)// shift OP
                begin
                if (cmd == 1'b0) errorFrame_nxt = 1'b1;
                else errorFrame_nxt = errorFrame;
                a_nxt = a_;
                b_nxt = b_;
                if (bitCnt >3 || bitCnt==0)op_nxt = op_;
                else op_nxt = {op_[1:0], sin};
                ctl_nxt = {ctl[6:0], sin};
                end
            else 
                begin
                errorFrame_nxt = errorFrame;
                a_nxt = a_;
                b_nxt = b_;
                op_nxt = op_;
                ctl_nxt = ctl;
                end            
            end
        else if ((state ==  STOP) && ( byteCnt == 8))//true after received last byte
        begin
            errorFrame_nxt = errorFrame;
            A_OUT_nxt = a_;
            B_OUT_nxt = b_;
            OP_OUT_nxt = op_;
            a_nxt = a_;
            b_nxt = b_;
            op_nxt = op_;
            dataReady_nxt = 1'b1;
            byteCnt_nxt = 5'b0;
            ctl_nxt = ctl;
            
        if (newcrc != ctl[3:0]) ERR_CRC_nxt = 1'b1;
          else ERR_CRC_nxt = ERR_CRC;     
             
        end
        else // cmd or start state, do nothing 
        begin
            errorFrame_nxt = errorFrame;
            A_OUT_nxt = A_OUT;
            B_OUT_nxt = B_OUT;
            OP_OUT_nxt = OP_OUT;
            a_nxt = a_;
            b_nxt = b_;
            op_nxt = op_;
            dataReady_nxt = 1'b0;
            ERR_CRC_nxt = ERR_CRC;
            ctl_nxt = ctl;
            if (state == STOP) 
                byteCnt_nxt = byteCnt + 5'b1;
            else byteCnt_nxt = byteCnt;
         end
      end   
  else//   if (baudClk == 1'b1)
     begin
     errorFrame_nxt = errorFrame;
     A_OUT_nxt = A_OUT;
     B_OUT_nxt = B_OUT;
     OP_OUT_nxt = OP_OUT;
     a_nxt = a_;
     b_nxt = b_;
     op_nxt = op_;
     dataReady_nxt = dataReady;
     ERR_CRC_nxt = ERR_CRC;
     ctl_nxt = ctl;
     byteCnt_nxt = byteCnt;
     end
end
//#########################     Assing frame error      #########################  
    
    always @*
    begin
        if (state == ERROR_FRAME) ERR_DATA_nxt = 1'b1;
        else ERR_DATA_nxt = 1'b0;
    end

    always @( posedge clk )
    begin
    if (rst_n == 1'b0) ERR_DATA <= 1'b0;
    else ERR_DATA <= ERR_DATA_nxt;
    end

endmodule
