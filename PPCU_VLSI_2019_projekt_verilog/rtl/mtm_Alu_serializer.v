`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.08.2019 17:45:53
// Design Name: 
// Module Name: deser
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mtm_Alu_serializer(
    input  wire clk,
    input  wire rst_n,
    input  wire [31:0] data, 
    input  wire [3:0] aluFlags,
    input  wire ERR_OP,    // bad CRC for the input data    
    input  wire ERR_CRC,    // bad CRC for the input data
    input  wire ERR_DATA,   // incorrect input data, wrong number of DATA frames before CTL frame (should always be 8), 
    input  wire dataReadyIn,  
    output reg sDataOut
    );
    
    
    localparam 
        TICK_PER_BIT = 10
     ;
     //assing sDataOut = sout_tmp[39];
//#########################      baud and reset generator    #########################
  
    //reg ERR_OP_SEND_nxt , ERR_OP_SEND, ERR_CRC_SEND, ERR_CRC_SEND_nxt, ERR_DATA_SEND, ERR_DATA_SEND_nxt;
    reg ERR_SENT_LOCK, ERR_SENT_LOCK_nxt; 
    reg sendDataAndCrc, sendDataAndCrc_nxt;
    //reg baudGenEn ,baudGenEn_nxt ,baudClk ,baudClk_nxt;
    reg sDataOut_nxt;
    reg [3:0] baudGenCnt, baudGenCnt_nxt;
    reg [39:0] sout_tmp, sout_tmp_nxt;  
    reg [5:0] messageBitCnt, messageBitCnt_nxt;
    reg [3:0] bitSend, bitSend_nxt;
    reg [2:0] crc, crc_nxt;

    always @( posedge clk )    
    if (rst_n == 1'b0) 
        begin
        ERR_SENT_LOCK <= 1'b0;
        baudGenCnt <= 4'b0;
        sout_tmp <= 40'b0;
        messageBitCnt <=6'b0;
        sendDataAndCrc<=1'b0;
        bitSend <=4'b0;
        sDataOut <=1'b1;
        crc <=3'b0;
        end
    else 
        begin 
        ERR_SENT_LOCK <= ERR_SENT_LOCK_nxt;
        baudGenCnt <= baudGenCnt_nxt;
        sout_tmp <= sout_tmp_nxt;
        messageBitCnt <= messageBitCnt_nxt;
        sendDataAndCrc <= sendDataAndCrc_nxt;
        bitSend <= bitSend_nxt;
        sDataOut <= sDataOut_nxt;
        crc <= crc_nxt;
        end
  
    always @*
    begin
    if (messageBitCnt == 0 && bitSend == 0 && ERR_SENT_LOCK == 0)//new packet
        begin
        sDataOut_nxt = sDataOut;
        bitSend_nxt = 4'b0;
        crc_nxt=3'b0;
        if( ERR_CRC == 1'b1)
            begin
            ERR_SENT_LOCK_nxt = 1'b1;
            baudGenCnt_nxt = 4'b0;
            //               1'b1 data crc  op   data crc  op   parity
            sout_tmp_nxt = { 1'b1,1'b0,1'b1,1'b0,1'b0,1'b1,1'b0,1'b1,32'b0 };
            messageBitCnt_nxt = 6'd8;
            sendDataAndCrc_nxt = 1'b0;
            end
        else if ( ERR_DATA == 1'b1)
            begin
            ERR_SENT_LOCK_nxt = 1'b1;
            baudGenCnt_nxt = 4'b0;
            //               1'b1 data crc  op   data crc  op   parity
            sout_tmp_nxt = { 1'b1,1'b1,1'b0,1'b0,1'b1,1'b0,1'b0,1'b1,32'b0 };
            messageBitCnt_nxt = 6'd8;
            sendDataAndCrc_nxt = 1'b0;
            end
        else if ( ERR_OP == 1'b1)
            begin
            ERR_SENT_LOCK_nxt = 1'b1;
            baudGenCnt_nxt=4'd0;
            //               1'b1 data crc  op   data crc  op   parity
            sout_tmp_nxt = { 1'b1,1'b0,1'b0,1'b1,1'b0,1'b0,1'b1,1'b1,32'b0 };
            messageBitCnt_nxt = 6'd8;
            sendDataAndCrc_nxt=1'b0;
            end
        else if (dataReadyIn == 1 )
            begin
            ERR_SENT_LOCK_nxt = 1'b0;
            baudGenCnt_nxt = 4'b0;
            sout_tmp_nxt = {data, 1'b0, aluFlags, 3'b000};
            messageBitCnt_nxt = 6'd37;
            sendDataAndCrc_nxt = 1'b1;
            end
        else
            begin //default
            ERR_SENT_LOCK_nxt = ERR_SENT_LOCK;
            baudGenCnt_nxt = baudGenCnt;
            sout_tmp_nxt = sout_tmp;
            messageBitCnt_nxt = messageBitCnt;
            sendDataAndCrc_nxt = sendDataAndCrc;
            end
        end
    else if (baudGenCnt >= TICK_PER_BIT-1) // send bit
        begin
        ERR_SENT_LOCK_nxt = ERR_SENT_LOCK;
        baudGenCnt_nxt = 4'd0;
        sendDataAndCrc_nxt = sendDataAndCrc;
        if(bitSend == 0)
            begin //start bit
            messageBitCnt_nxt = messageBitCnt;
            bitSend_nxt = bitSend +4'd1;
            crc_nxt = crc;
            sDataOut_nxt = 1'b0;
            sout_tmp_nxt = sout_tmp;
            end
        else if(bitSend == 1) //send CTL bit
            begin
            messageBitCnt_nxt = messageBitCnt;//add ctd bit
            bitSend_nxt = bitSend +4'd1;
            crc_nxt = crc;
            sDataOut_nxt = (messageBitCnt == 8)|(messageBitCnt == 5);// ~sendDataAndCrc;// 1- CTL 0- DATA
            sout_tmp_nxt = sout_tmp;
            end
        else if(messageBitCnt == 0 && sendDataAndCrc && bitSend <=9)
            begin//add crc at the end of message
            messageBitCnt_nxt = messageBitCnt;//add ctd bit
            bitSend_nxt = bitSend +4'd1;
            crc_nxt = {crc[1], crc[0], 1'b0};
            sDataOut_nxt = crc[2];
            sout_tmp_nxt = sout_tmp;
            end
        else if(bitSend <= 9)
            begin //send data
            messageBitCnt_nxt = messageBitCnt -6'd1;
            bitSend_nxt = bitSend +4'd1;
            sDataOut_nxt=sout_tmp[39];
            crc_nxt = {crc[1]^sDataOut,crc[0],crc[2]^sDataOut};
            sout_tmp_nxt = {sout_tmp[38:0], 1'b0};
            end
        else//stop bit
            begin
                messageBitCnt_nxt = messageBitCnt;
                if (ERR_SENT_LOCK == 1'b0) bitSend_nxt = 4'd0;
                else bitSend_nxt = bitSend;
                if(messageBitCnt == 0) crc_nxt = 3'd0;
                else crc_nxt = crc;
                sDataOut_nxt = 1'b1;
                sout_tmp_nxt = sout_tmp;
            end
        end
    else// do nothing
        begin
        ERR_SENT_LOCK_nxt = ERR_SENT_LOCK;
        baudGenCnt_nxt = baudGenCnt + 4'd1;
        sout_tmp_nxt = sout_tmp;
        messageBitCnt_nxt = messageBitCnt;
        sendDataAndCrc_nxt = sendDataAndCrc;
        sDataOut_nxt=sDataOut;
        bitSend_nxt = bitSend;
        crc_nxt = crc;            
        end
    end

endmodule 
