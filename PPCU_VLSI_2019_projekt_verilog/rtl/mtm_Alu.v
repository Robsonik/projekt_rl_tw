`timescale 1ns / 1ps
/******************************************************************************
 * (C) Copyright 2019 AGH UST All Rights Reserved
 *
 * MODULE:    mtm_Alu
 * PROJECT:   PPCU_VLSI
 * AUTHORS:
 * DATE:
 * ------------------------------------------------------------------------------
 * The ALU should operate as described in the mtmAlu_test_top module.
 * It should consist of three modules connected together:
 *   mtm_Alu_deserializer
 *   mtm_Alu_core
 *   mtm_Alu_serializer
 * The ALU should use posedge active clock and synchronous reset active LOW.
 *
 *******************************************************************************/

module mtm_Alu (
    input  wire clk,   // posedge active clock
    input  wire rst_n, // synchronous reset active low
    input  wire sin,   // serial data input
    output wire sout   // serial data output
);

wire [31:0] A_OUT, B_OUT, C;
wire dataReady, ERR_CRC, ERR_DATA, OP_ERROR,  dataReadyALU;
wire [2:0] OP_OUT;
wire [3:0] FLAGS;

mtm_Alu_core u_mtm_Alu_core(
    .clk(clk),   
    .rst_n(rst_n), 
	.A(A_OUT),
	.B(B_OUT),
	.OP(OP_OUT),
	.dataReadyIn(dataReady),
	.dataReadyOut(dataReadyALU),
    .C(C),   
	.OP_ERROR(OP_ERROR),
	.FLAGS(FLAGS)	
);

mtm_Alu_deserializer u_mtm_Alu_deserializer(
    .clk(clk),
    .rst_n(rst_n),
    .sin(sin),
    .dataReady(dataReady),
    .A_OUT(A_OUT),   
    .B_OUT(B_OUT),    
    .OP_OUT(OP_OUT),   
    .ERR_CRC(ERR_CRC),    
    .ERR_DATA(ERR_DATA)   
    );
    
mtm_Alu_serializer u_mtm_Alu_serializer(
    .clk(clk),
    .rst_n(rst_n),
    .data(C), 
    .aluFlags(FLAGS),
    .ERR_OP(OP_ERROR),    
    .ERR_CRC(ERR_CRC),    
    .ERR_DATA(ERR_DATA),
    .dataReadyIn(dataReadyALU),   
    .sDataOut(sout)
    );

endmodule
